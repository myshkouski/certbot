variable "REPO" {}
variable "VERSION" {
  default = "latest"
}

function "tag" {
  params = [version]
  result = ["${REPO}:${version}"]
}

group "default" {
  targets = [
    "build"
  ]
}

target "build" {
  context  = "./"
  args = {
    CERTBOT_VERSION = "${VERSION}"
  }
  platforms = [
    "linux/arm64/v8",
    "linux/amd64"
  ]
  tags = concat(
    tag("${VERSION}"),
    tag("${VERSION}-${formatdate("YYYYMMDDHHMM", timestamp())}")
  )
}
