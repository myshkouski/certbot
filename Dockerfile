ARG CERTBOT_VERSION=latest

FROM --platform=amd64 certbot/certbot:amd64-${CERTBOT_VERSION} as stage-amd64
FROM --platform=arm64/v8 certbot/certbot:arm64v8-${CERTBOT_VERSION} as stage-arm64-v8
FROM --platform=arm64 stage-arm64-v8 as stage-arm64

FROM --platform=${TARGETPLATFORM} stage-${TARGETARCH}${TARGETVARIANT:+-$TARGETVARIANT}
